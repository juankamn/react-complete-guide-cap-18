import { createSlice } from '@reduxjs/toolkit'

const initialCounterState = { counter: 0, showCounter: true };

const counterSlice = createSlice({
    name: 'counter',
    initialState: initialCounterState,
    // initialState: initialState,
    reducers: {
        increment(state) {
            // seems to be allowed but toolkit use other package called IMMER that detect code like this and return a new state and dont mutate the actual state
            state.counter++;
        },
        increase(state, action) {
            state.counter = state.counter + action.payload;
        },
        decrement(state) {
            state.counter--;
        },
        toggle(state) {
            state.showCounter = !state.showCounter;
        },
    }
});

export const counterActions = counterSlice.actions;

export default counterSlice.reducer;